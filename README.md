# Netbeans Sublime Theme

Base on the Theme from [Sublime plugin by Davaakhuu](http://plugins.netbeans.org/plugin/53826/sublime-plugin-by-davaakhuu).
This Theme is adapted for me.

## Install Theme

* Download Nerd Font **MesloLGS NF** from: https://github.com/romkatv/powerlevel10k#fonts at the best Option 2: Release Archive Download
* Install the font on your OS
* Download the **ZIP** File.  
* Before you import the new settings export all settings and save this as backup.  
* Go to Options and click the import button on the bottom.  
* Import the **ZIP** file.
* Change the Font to **MesloLGS NF** if is posible
 * Fonts and Colors > Syntax > Language: All Languages > Category: Default
 * Miscellaneous > Output > Font
 * Miscellaneous > Terminal > Font


## Base Colors

| Name       | RGB Values          | HEX Values |
|------------|---------------------|------------|
| gray-dark  | rgb(39, 40, 34)     | #272822    |
| gray-light | rgb(192, 192, 192)  | #c0c0c0    |
| gray       | rgb(117, 113, 94)   | #75715e    |
| blue       | rgb(102, 217, 239)  | #66d9ef    | 
| green      | rgb(166, 226, 46)   | #a6e22e    |
| pink       | rgb(249, 38, 114)   | #f92672    |
| orange     | rgb(253, 115, 31)   | #fd731f    |
| yellow     | rgb(230, 219, 116)  | #e6db74    |
| purple     | rgb(174, 129, 255)  | #ae81ff    |
| dark-green | rgb(0,139,139)      | #008b8b    |


## Export Content
This export contains:

* Fonts & Colors
    + Sublime-Theme
* Miscellaneous
    + Output

## Netbeans Bugs
Netbeans has a bug on CSS Whitespaces color. Please added this manual. The right color must be **gray**.

## my additional Settings

* Appearance > Look and Feel
 * FlatLaf Dark
* Miscellaneous > Terminal
 * Font: MesloLGS NF
 * Font Size: 13
 * Foreground Color: 131, 148, 150, 255
 * Background Color: 0, 40, 51, 255
 * Selection Background Color: 47, 101, 202
